using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitch : MonoBehaviour
{
    public GameObject MainCamera;
    public GameObject SecondCam;
    public GameObject CabineCamera;
    public GameObject MouffleCamera;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Alpha1))
        {
            Debug.Log("Keycode Z");
            MainCamera.SetActive(true);
            SecondCam.SetActive(false);
            CabineCamera.SetActive(false);
            MouffleCamera.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            Debug.Log("KeyCode Q");
            MainCamera.SetActive(false);
            SecondCam.SetActive(true);
            CabineCamera.SetActive(false);
            MouffleCamera.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha3))
        {
            MainCamera.SetActive(false);
            SecondCam.SetActive(false);
            CabineCamera.SetActive(true);
            MouffleCamera.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Alpha4))
        {
            MainCamera.SetActive(false);
            SecondCam.SetActive(false);
            CabineCamera.SetActive(false);
            MouffleCamera.SetActive(true);
        }

    }
}
